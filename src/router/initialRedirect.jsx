import { useNavigate } from "react-router-dom";
import Loading from "../../src/components/UI/Loading";
import { useContext, useEffect } from "react";
import { UserContext } from "../context/userContext";

const InitialRedirect = () => {
    const { user } = useContext(UserContext);
    const navigate = useNavigate();

    /* The `useEffect` hook is used to perform side effects in a functional component. In this case, it
    is used to redirect the user to a specific route based on the value of the `user` variable. */
    
    useEffect(() => {
        const redirect = () => {
            if (user) {
                navigate("/dashboard");
            } else {
                navigate("/auth/login");
            }
        };
        
        redirect();
    }, [])

    return (
        <div className="flex items-center justify-center w-screen h-screen">
            <Loading />
        </div>
    )
}

export default InitialRedirect;