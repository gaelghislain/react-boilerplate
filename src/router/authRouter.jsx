// eslint-disable-next-line no-unused-vars
import React, { useEffect } from "react";
import { Route, Routes } from "react-router-dom";
import Login from "../pages/auth/Login";
import InitialRedirect from "./initialRedirect";
import NotFound from "../pages/app/NotFoundScreen";
import ForgetPassword from "../pages/auth/ForgetPassword";
import ResetPassword from "../pages/auth/ResetPassword";
import ConfirmAccount from "../pages/auth/confirmAccount";

const AuthRouter = () => {

    return (
        <Routes>
            <Route path="/" Component={InitialRedirect} />
            {/* <Route path="/auth/login" Component={Login} />
            <Route path="/auth/password-forgot" Component={ForgetPassword} />
            <Route path="/auth/password-reset/:key" Component={ResetPassword} />
            <Route path="/auth/confirm-account/:key" Component={ConfirmAccount} /> */}
            <Route path='*' element={<NotFound />} />
        </Routes>
    )
};

export default AuthRouter;