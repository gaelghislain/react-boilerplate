/* eslint-disable no-unused-vars */
// eslint-disable-next-line no-unused-vars
import React, { useContext } from "react";
import Loading from "../components/UI/Loading";
import { UserContext } from "../context/userContext";
import AdminRouter from "./adminRouter";
import AuthRouter from "./authRouter";

const AppNavigation = () => {
    const { isLoading, user } = useContext(UserContext);

    const Navigator = () => {
        /* This code is used to conditionally render different components based on whether a
        user is logged in or not. */

        if (user) {
            return <AdminRouter />
        } else {
            return <AuthRouter />
        }
    }

    return (
        <>
            {
                isLoading ?
                    <div className="flex items-center justify-center w-screen h-screen">
                        <Loading />
                    </div> : <Navigator />
            }
        </>
    )
};

export default AppNavigation;