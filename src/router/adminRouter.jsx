import { Route, Routes } from "react-router-dom";
import AdminRouterConstant from "../constant/Admin/routerConstant";
import NotFound from "../pages/app/NotFoundScreen";

/* 
	The `AdminRouter` component is a React component that defines the routing configuration for the
	admin section of a web application.
*/
const AdminRouter = () => {
	return (
		<Routes>
			{AdminRouterConstant.map((route, index) => {
				return (
					<Route
						key={`${route.name}` + index}
						path={route.path}
						element={<route.screen />}
					/>
				);
			})}
			<Route path="*" element={<NotFound />} />
		</Routes>
	);
};

export default AdminRouter;
