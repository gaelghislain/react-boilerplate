// eslint-disable-next-line react/prop-types
function BellIcon({ size, color }) {
    return (
        <svg width={size} height={size} viewBox="0 0 13 17" fill={color ?? "currentColor"} xmlns="http://www.w3.org/2000/svg" className="transition duration-75 text-white group-hover:text-[#0A51A1]">
            <path d="M6.4 16.6111C5.51684 16.6068 4.80073 15.8943 4.792 15.0111H7.992C7.99369 15.225 7.9529 15.4371 7.872 15.6351C7.66212 16.1167 7.23345 16.468 6.72 16.5791H6.716H6.704H6.6896H6.6824C6.58945 16.5985 6.49492 16.6092 6.4 16.6111ZM12.8 14.2111H0V12.6111L1.6 11.8111V7.41115C1.55785 6.28244 1.81275 5.16245 2.3392 4.16315C2.86323 3.23636 3.75896 2.57825 4.8 2.35515V0.611145H8V2.35515C10.0632 2.84635 11.2 4.64155 11.2 7.41115V11.8111L12.8 12.6111V14.2111Z"
                fillRule="evenodd" />
        </svg>
    )
}
export default BellIcon;