// eslint-disable-next-line react/prop-types
function FilterIcon({ size, color }) {
    return (

        <svg width={size} height={size} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M4 6H14M1.5 1H16.5M6.5 11H11.5"
                stroke={color ?? "#344054"}
                strokeWidth="1.67" strokeLinecap="round" strokeLinejoin="round" />
        </svg>

    )
}
export default FilterIcon;