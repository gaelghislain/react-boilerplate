const PointSvg = ({ color, size }) => {
	return (
		<svg
			height={size}
			width={size}
			viewBox="0 0 24 24"
			fill={color ?? "currentColor"}
			xmlns="http://www.w3.org/2000/svg"
		>
			<g id="SVGRepo_bgCarrier" strokeWidth="0"></g>
			<g
				id="SVGRepo_tracerCarrier"
				strokeLinecap="round"
				strokeLinejoin="round"
			></g>
			<g id="SVGRepo_iconCarrier">
				<circle cx="12" cy="12" r="6" fill={color ?? "currentColor"}></circle>
			</g>
		</svg>
	);
};

export default PointSvg;
