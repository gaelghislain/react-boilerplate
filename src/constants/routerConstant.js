import InitialRedirect from "../../router/initialRedirect";

const AdminRouterConstant = [
	{
		name: "/",
		path: "/",
		screen: InitialRedirect,
	},
	{
		primary: true,
		icon: ChartIcon,
		name: "Dashboard",
		path: "/dashboard",
		screen: Dashboard,
	},
];

export default AdminRouterConstant;
